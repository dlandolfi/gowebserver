package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
)

func main() {

	const port = "8080"

	var rootDirPtr = flag.String("rootDir", "/root/gowebserver", "Use to change working directory")
	flag.Parse()

	mux := http.NewServeMux()

	mux.Handle("/", http.FileServer(http.Dir(*rootDirPtr+"/public")))
	mux.HandleFunc("/test", test)
	mux.Handle("/favicon.ico", http.NotFoundHandler())
	mux.Handle("/testt", http.NotFoundHandler())
	// mux.HandleFunc("/testpage", testpage)

	fmt.Println("Listening on port " + port + "...")
	log.Fatal(http.ListenAndServe(":"+port, mux))
}

// func index(w http.ResponseWriter, r *http.Request) {
// 	w.Header().Set("Content-Type", "text/html; charset=utf-8")
// 	io.WriteString(w, "indexxx")
// 	http.FileServer(http.Dir("."))
// }

func test(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "<h1>Test</h1>")
}

// func testpage(w http.ResponseWriter, r *http.Request) {
// 	http.ServeFile(w, r, "dog.jpg")
// }
